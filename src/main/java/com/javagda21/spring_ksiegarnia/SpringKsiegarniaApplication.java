package com.javagda21.spring_ksiegarnia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringKsiegarniaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringKsiegarniaApplication.class, args);
    }

}
