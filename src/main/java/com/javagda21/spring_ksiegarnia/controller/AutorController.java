package com.javagda21.spring_ksiegarnia.controller;

import com.javagda21.spring_ksiegarnia.model.Autor;
import com.javagda21.spring_ksiegarnia.model.Ksiazka;
import com.javagda21.spring_ksiegarnia.model.TypKsiazki;
import com.javagda21.spring_ksiegarnia.service.AutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Controller
public class AutorController {

    @Autowired
    private AutorService autorService;

    @GetMapping("/autor/list")
    public String getAutorList(Model model) {
        model.addAttribute("autorList", autorService.read());
        return "autorList";
    }

    @GetMapping("/autor/add")
    public String getautorAdd(){
        return "autorAdd";
    }

    @PostMapping("/autor/add")
    public String postKsiazkaAdd(@RequestParam(name = "imie") String imie,
                                 @RequestParam(name = "nazwisko") String nazwisko,
                                 @RequestParam(name = "rokUrodzenia") Integer rokUrodzenia,
                                 @RequestParam(name = "miejsceUrodzenia") String miejsceUrodzenia
//                                 @RequestParam(name = "typKsiazki") String typKsiazki
                                 ) {
        Autor autor = new Autor(null, imie, nazwisko, rokUrodzenia, miejsceUrodzenia,new HashSet<>());
        autorService.add(autor);

        return "redirect:/autor/list";
    }

    @GetMapping("/autor/remove")
    public String getAutorRemove(@RequestParam(name = "id") Long position) {
        autorService.remove(position);
        return "redirect:/autor/list";
    }

}
