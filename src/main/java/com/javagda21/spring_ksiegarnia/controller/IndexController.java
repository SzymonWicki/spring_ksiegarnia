package com.javagda21.spring_ksiegarnia.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class IndexController {

    @GetMapping("/index")
    private String getIndex() {
        return "index";
    }

    @GetMapping("/")
    private String postIndex() {
        return "redirect:/index";
    }
}
