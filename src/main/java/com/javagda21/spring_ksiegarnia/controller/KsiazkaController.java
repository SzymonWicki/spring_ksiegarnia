package com.javagda21.spring_ksiegarnia.controller;

import com.javagda21.spring_ksiegarnia.model.Ksiazka;
import com.javagda21.spring_ksiegarnia.model.TypKsiazki;
import com.javagda21.spring_ksiegarnia.service.KsiazkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class KsiazkaController {

    @Autowired
    private KsiazkaService ksiazkaService;

    @GetMapping("/ksiazka/list")
    public String getKsiazkaList(Model model) {
        model.addAttribute("ksiazkaList", ksiazkaService.read());
        return "ksiazkaList";
    }

    @GetMapping("/ksiazka/add")
    public String getKsiazkaAdd(@RequestParam(name = "id") Long autorId, Model model){
        model.addAttribute("typKsiazki", TypKsiazki.values());
        model.addAttribute("id", autorId);
        return "ksiazkaAdd";
    }

    @PostMapping("/ksiazka/add")
    public String postKsiazkaAdd(@RequestParam(name = "tytul") String tytul,
                                 @RequestParam(name = "rokWydania") Integer rokWydania,
                                 @RequestParam(name = "iloscStron") Integer iloscStron,
//                                 @RequestParam(name = "autor") String autor,
                                 @RequestParam(name = "typKsiazki") TypKsiazki typKsiazki,
                                 @RequestParam(name = "id") Long autorId
                                 ) {
        Ksiazka ksiazka = new Ksiazka(null, tytul, rokWydania, typKsiazki, iloscStron, null);
        ksiazkaService.add(ksiazka,autorId);

        return "redirect:/ksiazka/list";
    }

    @GetMapping("/ksiazka/remove")
    public String getKsiazkaRemove(@RequestParam(name = "id") Long position) {
        ksiazkaService.remove(position);
        return "redirect:/ksiazka/list";
    }

}
