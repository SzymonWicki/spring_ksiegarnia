package com.javagda21.spring_ksiegarnia.service;

import com.javagda21.spring_ksiegarnia.model.Autor;
import com.javagda21.spring_ksiegarnia.model.Ksiazka;
import com.javagda21.spring_ksiegarnia.repository.AutorRepository;
import com.javagda21.spring_ksiegarnia.repository.KsiazkaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class KsiazkaService {

    @Autowired
    private KsiazkaRepository ksiazkaRepository;

    @Autowired
    private AutorRepository autorRepository;

//    private List<Ksiazka> ksiazkas = new ArrayList<>(); // service musi byc bezstanowy wiec nie mozna tworzyc listy

    public void add(Ksiazka ksiazka, Long autorId) {
        final Optional<Autor> byId = autorRepository.findById(autorId);
        if (byId.isPresent()){
            ksiazka.setAutor(byId.get());
        } else {
            throw new EntityNotFoundException();
        }
        ksiazkaRepository.save(ksiazka);
    }

    public List<Ksiazka> read() {
        return ksiazkaRepository.findAll();
    }

    public void remove(Long position) {
        ksiazkaRepository.deleteById(position);
    }

    public void update(int id) {

    }
}
