package com.javagda21.spring_ksiegarnia.service;

import com.javagda21.spring_ksiegarnia.model.Autor;
import com.javagda21.spring_ksiegarnia.model.Ksiazka;
import com.javagda21.spring_ksiegarnia.repository.AutorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AutorService {

    @Autowired
    private AutorRepository autorRepository;

//    private List<Autor> autors = new ArrayList<>();

    public void add(Autor autor) {
        autorRepository.save(autor);
    }

    public List<Autor> read() {
        return autorRepository.findAll();
    }

    public void remove(Long position) {
        autorRepository.deleteById(position);
    }

    public void update(int id) {

    }
}
