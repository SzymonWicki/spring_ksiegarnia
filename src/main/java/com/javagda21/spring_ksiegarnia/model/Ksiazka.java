package com.javagda21.spring_ksiegarnia.model;

import com.javagda21.spring_ksiegarnia.model.TypKsiazki;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Ksiazka {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String tytul;
    private Integer rokWydania;
    @Enumerated(EnumType.STRING)
    private TypKsiazki typKsiazki;
    private Integer iloscStron;

    @ManyToOne
    private Autor autor;
}
