package com.javagda21.spring_ksiegarnia.model;

public enum TypKsiazki {

    PRZYGODOWA,
    AKCJI,
    NAUKOWA
}
