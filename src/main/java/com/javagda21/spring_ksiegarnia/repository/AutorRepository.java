package com.javagda21.spring_ksiegarnia.repository;

import com.javagda21.spring_ksiegarnia.model.Autor;
import com.javagda21.spring_ksiegarnia.model.Ksiazka;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AutorRepository extends JpaRepository<Autor, Long> {
}
